
#include <cstdio>
#include <cstring>
#include <cstdint>
//#include <stdint.h> // Use this instead of cstdint so that it works on Mac.
#include "chacha20_simple.h"
#include "crypto.h"

/* The keyfile should define symmetric_key as the symmetric
 * key and iv as the initializing vector */
#include "keyfile.h"

const size_t BUFFER_SIZE = 256;

static chacha20_ctx ctx;
static uint8_t buffer[BUFFER_SIZE];

bool encrypt(const char *p, char *c, size_t buffer_size)
{
	size_t len = std::strlen(p);
	if (buffer_size < len * 2 + 1 || BUFFER_SIZE < len * 2 + 1)
		return false;
	chacha20_setup(&ctx, symmetric_key, 32, iv);
	chacha20_encrypt(&ctx, (uint8_t*)p, buffer, len);
	for (size_t i = 0; i < len; i++)
		std::sprintf(c + 2 * i, "%2X", buffer[i]);
	return true;
}

bool decrypt(char *p, const char *c, size_t buffer_size)
{
	size_t len = strlen(c);
	if (len % 2 != 0)
		return false;
	len /= 2;
	if (buffer_size < len + 1 || BUFFER_SIZE < len + 1)
		return false;
	for (size_t i = 0; i < len; i++)
		std::sscanf(c + 2 * i, "%2X", buffer + i);
	chacha20_setup(&ctx, symmetric_key, 32, iv);
	chacha20_decrypt(&ctx, buffer, (uint8_t*)p, len);
	p[len] = '\0';
	return true;
}

void get_client_key(uint8_t key[32]){}

void set_client_key(const uint8_t key[32]){}

