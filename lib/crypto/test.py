from ctypes import *
import os
import sys

dylib_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'crypto.so')
lib_c = cdll.LoadLibrary(dylib_path)

SIZE = 256

lib_c.encrypt.argtypes = [c_char_p, c_char_p, c_int]
lib_c.encrypt.restype = c_bool
lib_c.decrypt.argtypes = [c_char_p, c_char_p, c_int]
lib_c.decrypt.restype = c_bool

def encrypt(text):
    cipher = create_string_buffer(SIZE)
    if lib_c.encrypt(text, cipher, SIZE):
        return cipher.value
    else:
        raise Error

def decrypt(cipher):
    text = create_string_buffer(SIZE)
    if lib_c.decrypt(text, cipher, SIZE):
        return text.value
    else:
        raise Error

if __name__ == '__main__':
    text = "hello world"
    print " Original text: {}".format(text)
    cipher = encrypt(text)
    print "Encrypted text: {}".format(cipher)
    decrypted_text = decrypt(cipher)
    print "Decrypted text: {}".format(decrypted_text)
    print "Equal: {}".format(text == decrypted_text)
    sys.exit(text == decrypted_text)
