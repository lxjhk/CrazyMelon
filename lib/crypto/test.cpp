
#include <iostream>
#include "crypto.h"

int main()
{
	static char p[256];
	static char c[256];
	std::cin.getline(p,256);
	encrypt(p,c,256);
	std::cout << c << std::endl;
	decrypt(p,c,256);
	std::cout << p << std::endl;
	return 0;
}
