
#ifndef _CRYPTO_H_
#define _CRYPTO_H_

#include <cstring>

#ifdef __cplusplus
extern "C"
{
#endif

/*
 * p:           plain text
 * c:           cipher
 * buffer_size: maximum number of bytes to write into c
 */
bool encrypt(const char *p, char *c, size_t buffer_size);

/*
 * p:           plain text
 * c:           cipher
 * buffer_size: maximum number of bytes to write into p
 */
bool decrypt(char *p, const char *c, size_t buffer_size);

/*
 * reserved
 */
void get_client_key(uint8_t k[32]);

/*
 * reserved
 */
void set_client_key(const uint8_t k[32]);

#ifdef __cplusplus
}
#endif

#endif
