
// This is a sample keyfile for debugging
// This file should not be used in production

#ifndef _KEYFILE_H_
#define _KEYFILE_H_

/* This keyfile should define symmetric_key as the symmetric
 * key and iv as the initializing vector */

#include <cstdint>
// #include <stdint.h>

uint8_t symmetric_key[32] = {0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31};
uint8_t iv[8] = {7,6,5,4,3,2,1,0};

#endif
