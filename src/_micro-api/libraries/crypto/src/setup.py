#!/usr/bin/env python

"""
    Set up the python binding for our encryption library
    Created by Yicheng Luo on 8 June, 2016.
    on how to write the setup script, visit:
    https://docs.python.org/2/distutils/setupscript.html
"""

from setuptools import setup
from setuptools.extension import Extension
import os

package = 'button_crypto'
version = '0.1'

setup(name=package,
      version=version,
      ext_modules=[Extension(name='crypto',
                             sources=['crypto.cpp', 'chacha20_simple.c'],
                             language='c++',
                             extra_compile_args=['-std=c++11']
                             )])
