//CrazyMelon Project

#include <OneWire.h>
#include <EtherCard.h>
#include <avr/pgmspace.h>
#include <avr/io.h>
#include <Arduino.h>


const static byte devID[] PROGMEM = { 0x00,0x69,0x69,0x2D,0x30,0x31 };
#define gasPin A0 //GAS sensor output pin to Arduino analog A0 pin

// ethernet interface mac address, must be unique on the LAN
const static byte mymac[] = { 0x74,0x69,0x69,0x2D,0x30,0x31 };
byte Ethernet::buffer[500];
static uint32_t timer;

// called when the client request is complete
static void my_callback(byte status, word off, word len) {
	Serial.println(F(">>>"));
	Ethernet::buffer[off + 300] = 0;
	Serial.print((const char*)Ethernet::buffer + off);
	Serial.println(F("..."));
}

String BytestoHexString(const byte* bytesArray) {
	String returnString = "";
	for (int i = 0; i <= 5; i++) 
		if (String(pgm_read_byte(&bytesArray[i]), HEX).length() == 1)
			returnString += "0" + String(pgm_read_byte(&bytesArray[i]), HEX);
		else
			returnString += String(pgm_read_byte(&bytesArray[i]), HEX);

	return returnString;
}



void setup() {
	//Serial Communication Setup
	Serial.begin(57600);
	Serial.println(F("\n[webClient]"));
	Serial.println(BytestoHexString(devID));

	//EtherCard Setup
	while (ether.begin(sizeof Ethernet::buffer, mymac) == 0)
		Serial.println(F("Failed to access Ethernet controller"));

	while (!ether.dhcpSetup()) 
		Serial.println(F("DHCP failed"));

	ether.printIp("IP:  ", ether.myip);
	ether.printIp("GW:  ", ether.gwip);
	ether.printIp("DNS: ", ether.dnsip);
	ether.hisip[0] = 192;
	ether.hisip[1] = 168;
	ether.hisip[2] = 1;
	ether.hisip[3] = 100;
	ether.hisport = 8081;
	ether.printIp("SRV: ", ether.hisip);


}


void loop() {

	ether.packetLoop(ether.packetReceive());
	Serial.println(analogRead(gasPin));

	if (millis() > timer) {
		timer = millis() + 3000;
		Serial.println(	F("<<< REQ "));
		//ether.browseUrl(PSTR("/"), "", NULL, my_callback);
	}

}